<?php

namespace Drupal\disable_user_1\EventSubscriber;

use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 *
 */
class DisableUser1EventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest'];
    return $events;
  }

  /**
   * Redirect logic.
   */
  public function onRequest(RequestEvent $events) {
    // Check for a configuration setting in settings.php.
    $disable_user_1 = \Drupal::config('disable_user_1.settings')->get('disable_user_1');
    // If the configuration setting is TRUE, prevent user 1 from logging in.  
    $current_user_id = \Drupal::currentUser()->id();
    if ($disable_user_1 && $current_user_id == 1) {
      user_logout();
      \Drupal::messenger()->addError(t('User 1 is disabled and cannot log in.'));
      $response = new \Symfony\Component\HttpFoundation\RedirectResponse('/');
      $response->send();
      return;
    }
  }

}
